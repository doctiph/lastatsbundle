<?php

namespace La\StatsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="lastats__stats")
 */
class Stats
{

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $label;

    /**
     * @ORM\Column(type="text")
     */
    protected $stats;

    /**
     * @ORM\Column(type="text")
     */
    protected $stattype;

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $stats
     */
    public function setStats($stats)
    {
        $this->stats = $stats;
    }

    /**
     * @return mixed
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * @param mixed $stattype
     */
    public function setStattype($stattype)
    {
        $this->stattype = $stattype;
    }

    /**
     * @return mixed
     */
    public function getStatType()
    {
        return $this->stattype;
    }

}