<?php

namespace La\StatsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LaStatsExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (is_null($config['tmp_dir'])) {
            $config['tmp_dir'] = sprintf('%s/tmp', $container->getParameter('kernel.root_dir'));
        }

        $container->setParameter('la_stats.tmp_dir', $config['tmp_dir']);
        if (!file_exists($config['tmp_dir'])) {
            mkdir($config['tmp_dir']);
        }

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/services'));
        $loader->load('services.xml');

    }
}
