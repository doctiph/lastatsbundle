<?php
namespace La\StatsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use La\StatsBundle\Event\StatsEvent;

/**

php app/console stats:compute [type]

 */
class StatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stats:compute')
            ->setDescription('Aggregate available stats of a given type to persist them in database.')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Type of stats requested by the command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $type = $input->getArgument('type');

        try {

            $output->writeln(sprintf("Computing %s stats", $type));

            $event = new StatsEvent($type);
            $this->getContainer()
                ->get('event_dispatcher')
                ->dispatch(StatsEvent::STATS_COMPUTE, $event);

            if($event->isPropagationStopped()){
//                throw new \Exception($event->getError());
                $output->writeln($event->getError());
            }

        } catch (\Exception $e) {
            $output->setVerbosity(1);
            $output->writeln(sprintf("Error : %s", $e->getMessage()));
        }
    }

}
