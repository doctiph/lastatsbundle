<?php

namespace La\StatsBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\StatsBundle\Event\StatsEvent;
use La\StatsBundle\Model\StatsManagerInterface;

class StatsListener implements EventSubscriberInterface
{
    /**
     * @var StatsManagerInterface $manager
     */
    protected $manager;

    public function __construct(StatsManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            StatsEvent::STATS_REQUEST => array('onStatsRequest'),
            StatsEvent::STATS_COMPUTE => array('onStatsCompute')
        );
    }

    /**
     * @param StatsEvent $event
     */
    public function onStatsRequest(StatsEvent $event)
    {
        if ($this->shouldAnswer($event)) {
            $event->setStats($this->manager->getStats());
        }
    }

    /**
     * @param StatsEvent $event
     */
    public function onStatsCompute(StatsEvent $event)
    {
        if ($this->shouldAnswer($event)) {
            try {
                $this->manager->insertStats();
            } catch (\Exception $e) {
                $event->setError($e->getMessage());
            }
        }
    }

    /**
     * Check if current listener should answer to the event or not
     *
     * @param StatsEvent $event
     * @return bool
     */
    protected function shouldAnswer(StatsEvent $event)
    {
        return $event->getType() === $this->manager->getType();
    }

}