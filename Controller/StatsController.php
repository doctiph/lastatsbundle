<?php

namespace La\StatsBundle\Controller;

use La\StatsBundle\Model\StatsExportManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatsController
 * @package La\StatsBundle\Controller
 */
class StatsController extends Controller
{

    /**
     * @param Request $request
     * @param $type
     * @return Response
     */
    public function displayAction(Request $request, $type)
    {
        $bundleName = ucfirst(strtolower($type));
        $template = sprintf('La%sBundle:Stats:index.html.twig', $bundleName);

        if ($this->get('templating')->exists($template)) {
            return $this->container->get('templating')->renderResponse($template, array(
                'type' => $type,
                'layout' => "LaStatsBundle:Display:layout.html.twig"
            ));
        } else {
            return $this->container->get('templating')->renderResponse('LaStatsBundle:Display:error.html.twig', array('type' => $type));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function exportAction(Request $request)
    {
        $exportManager = $this->get('la_stats.export.manager');
        $mime = $request->request->get('type');
        $now = new \DateTime();
        $filename = sprintf('%s_chart.%s', $now->format('Ymd'), $exportManager->getExt($mime));
        try {
            $response = new Response($exportManager->export($mime, $request->request->get('svg')), 200);
            $response->headers->set('Content-Type', $mime);
            $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));
            return $response;

        } catch (\Exception $e) {
            return $this->container->get('templating')->renderResponse('LaStatsBundle:Export:error.html.twig', array('error' => $e->getMessage()));
        }
    }

}
