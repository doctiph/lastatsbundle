<?php

namespace La\StatsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use La\StatsBundle\Event\StatsEvent;
use La\ApiBundle\Controller\AbstractApiController;

class ApiStatsController extends AbstractApiController
{
    const KEY = 'stats';

    /**
     * @Rest\View()
     * @Rest\Get("{key}")
     */
    public function getStatsAction(Request $request, $key)
    {
        try {
            $event = new StatsEvent($key);
            $this->container
                ->get('event_dispatcher')
                ->dispatch(StatsEvent::STATS_REQUEST, $event);

            if ($event->isPropagationStopped()) {
                throw new \Exception($event->getError());
            }

            $result = $event->getStats();

            // If a callback is passed, format in jsonp
            if ($request->query->has('callback')) {

                $content = sprintf('%s(%s);',
                    $request->query->get('callback'),
                    json_encode($result)
                );

                $response = new Response($content);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            // Else return json
            return $this->send($result, Codes::HTTP_OK);
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }


}