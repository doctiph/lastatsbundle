<?php

namespace La\StatsBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class StatsEvent extends Event
{
    /**
     * The STATS_REQUEST event occurs when stats are requested.
     *
     * The event listener method receives a La\StatsBundle\Event\StatsEvent instance.
     */
    const STATS_REQUEST = "la.stats.request";

    /**
     * The STATS_COMPUTE event occurs when stats are computed. Each bundle that can or should provide stats must listen to it.
     *
     * The event listener method receives a La\StatsBundle\Event\StatsEvent instance.
     */
    const STATS_COMPUTE = "la.stats.compute";

    /**
     * Type of stats the event is handling. Allow listeners to know if they should provide stats.
     *
     * @var string
     */
    protected $type;

    /**
     * Stats provided by listeners.
     *
     * @var array
     */
    protected $stats = [];

    /**
     * Error(s) provided by listeners.
     *
     * @var string
     */
    protected $error = null;


    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param $stats
     */
    public function setStats($stats)
    {
        $this->stats = $stats;
    }

    /**
     * @return array
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * @param $error
     */
    public function setError($error)
    {
        $this->error = $error;
        // If an error is set, stop propagation.
        $this->stopPropagation();
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

}