** STATS MANAGER **

Register your manager as a service. Eg for UserBundle :

        <service id="la_user.stats.manager" class="La\UserBundle\Services\StatsManager">
            <argument type="service" id="doctrine.orm.entity_manager"/>
        </service>

** LISTENER **

Register a listener with your manager. Use StatsBundle listener. Eg for UserBundle :

        <service id="la_user.stats.listener" class="La\StatsBundle\EventListener\StatsListener">
            <tag name="kernel.event_subscriber"/>
            <argument type="service" id="la_user.stats.manager"/>
        </service>


** CONFIG **

Créer l'utilisateur spécifique qui aura les droits sur la route /api/stats/*

### app/config/la_config.yml

    la_api:
        clients:
            stats_sitename:
                password: [unpasswordgénéréaléatoirement]
                role: ROLE_API_STATS

### app/security.yml

    access_control:
        ....
        - { path: ^/api/stats, role: ROLE_API_STATS }


** CRON **

Ce cron parcourt les tables user du site, consolide des stats et les stocke afin de les rendre accessible sur profile.sitename.tld/api/stats/<type>/display
Attention : La route /api/stats/<type>/display n'est accessible qu'à un utilisateur ayant le rôle ROLE_API_STATS. Pensez à vérifier  votre config (la_api : clients  dans /app/config.yml)

9 0 * * *    www-data    php app/console stats:compute <type> -e prod

Exemple pour les stats user :

9 0 * * *    www-data    php app/console stats:compute user -e prod



