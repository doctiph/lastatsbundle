LaStatsBundle
============

# Installation

# composer.json

    "repositories": [
        ....
        {
            "type": "git",
            "url": "git@git-pool01.in.ladtech.fr:LaStatsBundle"
        }
    ],


 "require": {
        ....
        "la/statsbundle": "dev-master"
    },



# RegisterBundle
Enregistrer les bundles dans app/AppKernel.php si ce n'est pas déjà fait.

    public function registerBundles()
    {
        $bundles = array(
            // ....
            new La\StatsBundle\LaStatsBundle(),
            // ....
        );
    }

# Import des fichiers de routing

### app/config/routing.yml

    # stats
    la_stats:
        resource: "@LaStatsBundle/Resources/config/routing/routing.yml"
        prefix:   /admin/stats #ou toutes autre route...


# STATS MANAGER

##  Register your manager as a service. Eg for UserBundle :

        <service id="la_user.stats.manager" class="La\UserBundle\Services\StatsManager">
            <argument type="service" id="doctrine.orm.entity_manager"/>
        </service>

# LISTENER

## Register a listener with your manager. Use StatsBundle listener. Eg for UserBundle :

        <service id="la_user.stats.listener" class="La\StatsBundle\EventListener\StatsListener">
            <tag name="kernel.event_subscriber"/>
            <argument type="service" id="la_user.stats.manager"/>
        </service>


# CRON

Ce cron parcourt les tables user du site, consolide des stats et les stocke.

9 0 * * *    www-data    php app/console stats:compute <type> -e prod

Exemple pour les stats user :

9 0 * * *    www-data    php app/console stats:compute user -e prod



