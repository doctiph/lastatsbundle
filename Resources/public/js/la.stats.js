/*global document */

var US, url_stats_prefix = url_stats_prefix || '';

US = (function () {
    "use strict";

    /*global url_stats_prefix, window, console, XMLHttpRequest, JSON, US, jQuery, Highcharts, alert */
    /*jslint todo: true */

    return {

        JSONurlPostfix: '',
        JSONurlPrefix: url_stats_prefix.concat('/api'),
        statsType: null,

        formatBarsStats: function formatBarsStats(baseData, destinationData, datetime) {

            var i, j, len = baseData.length, currentDataSet, prop, firstDate;

            //  pour les 3 sets en barres
            for (i = 0; i < len; i += 1) {

                j = 0;
                currentDataSet = baseData[i].stats;

                for (prop in currentDataSet) {

                    if (currentDataSet.hasOwnProperty(prop)) {

                        //  get first day date time to generate xAxis data
                        if (j < 1 && datetime) {

                            destinationData.series[i].pointStart = new Date(prop).getTime();
                            destinationData.series[i].pointInterval = 24 * 3600 * 1000;

                        } else if (!datetime) {
                            destinationData.xAxis.categories[j] = prop;
                        }
//                        destinationData.series[i].data[j] = ( +currentDataSet[prop] ) >> 0; // C'est quoi ce machin ?
                        destinationData.series[i].data[j] = +currentDataSet[prop];// C'est quoi ce machin ?
                        j += 1;

                    }

                }

            }

        },
        formatLineStats: function formatLineStats(baseData, destinationData, datetime) {

            var i, j = 0, len = baseData.stats.length, currentDataSet, currentDataSetValues, prop;

            //  pour les 3 sets en barres
            for (i = 0; i < len; i += 1) {

                j = 0;
                currentDataSet = baseData.stats[i];
                currentDataSetValues = currentDataSet.value;


                destinationData.series[i] = {
                    name: currentDataSet.name,
                    data: []
                };

                if (i > 4) {
                    destinationData.series[i].visible = false;
                }

                for (prop in currentDataSetValues) {

                    if (currentDataSetValues.hasOwnProperty(prop)) {

                        //  get first day date time to generate xAxis data
                        if (j < 1 && datetime) {

                            destinationData.series[i].pointStart = new Date(prop).getTime();
                            destinationData.series[i].pointInterval = 24 * 3600 * 1000;

                        } else if (!datetime) {
                            destinationData.xAxis.categories[j] = prop;
                        }

//                        destinationData.series[i].data[j] = ( +currentDataSetValues[prop] ) >> 0;
                        destinationData.series[i].data[j] = +currentDataSetValues[prop];
                        j += 1;

                    }

                }

            }

        },
        formatThousands: function formatThousands(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },

        onChartDataReady: function onChartDataReady() {
            console.error("This function must be overridden by stats-specific js.");
        },
        drawChartsFromData: function drawChartsFromData(chartsData) {
            console.error("This function must be overridden by stats-specific js.");
        },

        //  fetch stats data from API
        getDistantData: function getDistantData() { // requestUrl, postData, successCallBack, errorCallback

            var url = US.JSONurlPrefix.concat('/', US.statsType, US.JSONurlPostfix);
            jQuery.ajax({
                type: "GET",
                url: url,
                dataType: "jsonp",
                jsonp: 'callback',
                timeout: 5000
            })
                .done(function (successData) {
                    US.drawChartsFromData(successData);
                })
                .fail(function (jqXHR, textStatus) {
                    jQuery(document).trigger("chartDataError");
                });

        },


        getDataFromLabel: function (chartsData, label) {
            var length = chartsData.length,
                i,
                currentDataSet = null;

            if (chartsData && length > 0) {
                //extract datasets to treat them separately : month, totals, yearly
                for (i = 0; i < length; i += 1) {
                    currentDataSet = chartsData[i];
                    switch (currentDataSet.label) {
                        case label:
                            return currentDataSet;
                    }
                }

            }
            //  trigger error event -> fallback
            jQuery(function () {
                jQuery(document).trigger("chartDataError");
            });

        },

        getDefaultMonthChartsData: function () {
            return {
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { day: '%e/%m' },
                    labels: {
                        minorTickLength: 10
                    }
                },
                series: []
            };
        },

        getDefaultYearChartsData: function () {
            return {
                xAxis: {
                    categories: []
                },
                series: []
            };
        },

        init: function init(statsType) {

            US.statsType = statsType;
            if (US.statsType === undefined || US.statsType === null) {
                console.warn("Incorrect stats initialization. No stats type provided.");
            } else {
                console.log("Loading ".concat(US.statsType, " stats"));
            }

            //  attach listener for custom chartDataReady event :
            //  data ready for chart drawing
            jQuery(document).on("chartDataReady", function (event) {

                var highchartsOptions = Highcharts.setOptions({
                        lang: {
                            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                            weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                            shortMonths: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                            exportButtonTitle: "Exporter",
                            printButtonTitle: "Imprimer",
                            rangeSelectorFrom: "De",
                            rangeSelectorTo: "à",
                            rangeSelectorZoom: "Periode",
                            downloadPNG: 'Exporter en PNG',
                            downloadJPEG: 'Exporter en JPEG',
                            downloadPDF: 'Exporter en PDF',
                            downloadSVG: 'Exporter en SVG',
                            downloadCSV: 'Exporter en CSV'
                        },
                        exporting: {
                            url: url_stats_prefix.concat('/export')
                        }
                    }),
                    chartInstanceSetting = {
                        legend: {
                            align: 'center',
                            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: true
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:14px;text-align:center;width:100%;">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        }
                    };

                // allow custom behaviour
                US.onChartDataReady(chartInstanceSetting);
            });

            //  attach listener for custom chartDataError event :
            //  data not available
            jQuery(document).on("chartDataError", function (event) {
                alert('Données non disponibles');
            });

            //  get data ( callbacks US.formatData or trigger chartDataError event )
            US.getDistantData();
        }
    };

}());
