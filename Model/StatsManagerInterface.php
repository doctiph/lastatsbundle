<?php

namespace La\StatsBundle\Model;

interface StatsManagerInterface
{
    /**
     * Return stats type handled by this manager.
     * @return string
     */
    function getType();

    /**
     * Return available stats for this manager.
     * @return array("label"=>getter(), ...)
     */
    function listAvailableStats();

    /**
     * Fetch stats
     * @return array
     */
    function getStats();

    /**
     * Insert stats
     * @return void
     * @throws \Exception
     */
    function insertStats();
}