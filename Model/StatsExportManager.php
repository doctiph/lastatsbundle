<?php

namespace La\StatsBundle\Model;

class StatsExportManager
{


    /**
     * @var string
     */
    protected $tmpDir;


    public function __construct($tmpDir)
    {
        $this->tmpDir = $tmpDir;
    }

    /**
     * @param $mime
     * @return null|string
     * @throws \Exception
     */
    public function getExt($mime)
    {
        $ext = null;
        switch ($mime) {
            case 'text/csv':
                $ext = 'csv';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'application/pdf':
                $ext = 'pdf';
                break;
            case 'image/svg+xml':
                $ext = 'svg';
                break;
            default:
                throw new \Exception(sprintf("Export to '%s' format is not supported.", $mime));
        }
        return $ext;
    }

    /**
     * @param $mime
     * @param $source
     * @return string
     * @throws \Exception
     */
    public function export($mime, $source)
    {
        $ext = $this->getExt($mime);

        if (!file_exists('export')) {
            mkdir('export');
        }

        $tempName = md5(rand());
        $tmpFile = sprintf("%s/%s.svg", $this->getTmpDir(), $tempName);
        $outfile = sprintf("%s/%s.%s", $this->getTmpDir(), $tempName, $ext);

        if (in_array($ext, array('png', 'jpg', 'pdf'))) {

            // check for malicious attack in SVG
            if (strpos($source, "<!ENTITY") !== false || strpos($source, "<!DOCTYPE") !== false) {
                throw new \Exception("Suspicious export. Please contact the administrator.");
            }

            // generate the temporary SVG file
            if (!file_put_contents($tmpFile, $source)) {
                throw new \Exception("Couldn't create temporary file. Please contact the administrator.");
            }
            // do the conversion
            $command = sprintf("java -jar %s/batik/batik-rasterizer.jar -m %s -d %s %s", __DIR__ . '/../Resources/jar', $mime, $outfile, $tmpFile);
            $result = shell_exec($command);
            // catch error
            if (!is_file($outfile) || filesize($outfile) < 10) {
                throw new \Exception("Error while generating file. Please contact the administrator.");
            }
            chmod($tmpFile, 0777);
            chmod($outfile, 0777);
        }

        if (in_array($ext, array('png', 'jpg', 'pdf'))) {
            $output = file_get_contents($outfile);
        } else {
            $output = $source;
        }
        @unlink($tmpFile);
        @unlink($outfile);

        return $output;
    }


    public function setTmpDir($tmpDir)
    {
        $this->tmpDir = $tmpDir;
    }

    protected function getTmpDir()
    {
        return $this->tmpDir;
    }


}

