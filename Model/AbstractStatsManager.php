<?php

namespace La\StatsBundle\Model;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Monolog\Logger;

/**
 * Class AbstractStatsManager
 * @package La\StatsBundle\Model
 */
abstract class AbstractStatsManager implements StatsManagerInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Monolog\Logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $today;
    /**
     * @var string
     */
    protected $oneMonthAgo;
    /**
     * @var string
     */
    protected $oneYearAgo;


    /**
     * @param EntityManager $em
     * @param \Monolog\Logger $logger
     */
    public function __construct(EntityManager $em, Logger $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->today = $this->getDate();
        $this->oneMonthAgo = $this->getDate("-1 month");
        $this->oneYearAgo = $this->getDate("-1 year");
    }


    /**
     * @return string
     */
    public function getStatsEntity()
    {
        return "LaStatsBundle:Stats";
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function getStats()
    {
        $query = $this->em->getRepository($this->getStatsEntity())
            ->createQueryBuilder('a')
            ->select('a')
            ->where('a.stattype = :stattype')
            ->setParameter('stattype', $this->getType())
            ->getQuery();

        $stats = $query->getResult(Query::HYDRATE_ARRAY);
        /** cleanup */
        foreach ($stats as $i => $stat) {
            $cleanStats = @unserialize($stat['stats']);
            if ($cleanStats !== false) {
                $stats[$i]['stats'] = $cleanStats;
            }
        }

        return $stats;
    }

    /**
     * @param $label
     * @return array
     */
    public function getStat($label)
    {
        return $this->em->getRepository($this->getStatsEntity())->findBy(array("label" => $label, 'stattype' => $this->getType()));
    }

    /**
     * @throws \Exception
     */
    public function insertStats()
    {
        $this->logger->debug(sprintf("-- %s --", get_class($this)));

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $tableName = $this->em->getClassMetadata($this->getStatsEntity())->getTableName();
        $insertDate = new \DateTime();

        foreach ($this->listAvailableStats() as $label => $closure) {

            $time = new \DateTime();
            $this->logger->debug(sprintf("[%s] Inserting \"%s\"",$time->format("H:i:s"),$label));

            if (count($this->getStat($label))) {
                // UPDATE
                $res = $this->em->getConnection()->update(
                    $tableName,
                    array(
                        'updated' => $insertDate->format('Y-m-d H:i:s'),
                        'stats' => $closure,
                        'stattype' => $this->getType()
                    ),
                    array('label' => $label)
                );
            } else {
                // INSERT
                $res = $this->em->getConnection()->insert(
                    $tableName,
                    array(
                        'updated' => $insertDate->format('Y-m-d H:i:s'),
                        'label' => $label,
                        'stats' => $closure,
                        'stattype' => $this->getType()
                    )
                );
            }

            // If an error occured while inserting or updating, throw exception
            if ($res != 1) {
                throw new \Exception("An error occured while inserting stats in " . $tableName);
            }

        }
    }

    /**
     * @param $data
     * @return array
     */
    protected function populateMonthlyData($data)
    {
        $data = $this->reindexDataWithDate($data);
        $index = new \DateTime($this->oneMonthAgo);
        $today = new \DateTime($this->today);

        $populatedMonth = [];
        while ($index <= $today) {
            $indexDate = $index->format('Y-m-d');
            $populatedMonth[$indexDate] = isset($data[$indexDate]) ? $data[$indexDate] : 0;
            $index->modify("+1 day");
        }

        return $populatedMonth;
    }

    /**
     * @param $data
     * @return array
     */
    protected function populateYearlyData($data)
    {
        $data = $this->reindexDataWithDate($data);

        $index = new \DateTime($this->oneYearAgo);
        $today = new \DateTime($this->today);

        $populatedYear = [];
        while ($index <= $today) {
            $indexDate = $index->format('Y-m');
            $populatedYear[$indexDate] = isset($data[$indexDate]) ? $data[$indexDate] : 0;
            $index->modify("+1 month");

            if ($index > $today) {
                break;
            }
        }
        return $populatedYear;
    }

    protected function reindexDataWithDate($data)
    {
        $reindexedData = [];
        foreach ($data as $entry) {
            if (isset($entry['date']) && isset($entry['nb'])) {
                $reindexedData[$entry['date']] = $entry['nb'];
            }
        }
        return $reindexedData;
    }


    /**
     * @param string $modify
     * @return string
     */
    protected function getDate($modify = null)
    {
        $date = new \DateTime();
        if ($modify) {
            $date->modify($modify);
        }
        return $date->format('Y-m-d 23:59:59');
    }

}